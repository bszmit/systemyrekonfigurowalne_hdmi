`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    01:28:41 04/29/2015 
// Design Name: 
// Module Name:    multiplekser_module 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module multiplekser_module(
	input [2:0] switch_input,

	input [7:0] rx_red,
	input [7:0] rx_green,
	input [7:0] rx_blue,

	input [7:0] ycbcr_Y,
	input [7:0] ycbcr_Cb,
	input [7:0] ycbcr_Cr,

	input [7:0] bin,

	input rx_de,
	input rx_hsync,
	input rx_vsync,

	input ycbcr_de,
	input ycbcr_hsync,
	input ycbcr_vsync,

	output [7:0] tx_red,
	output [7:0] tx_green,
	output [7:0] tx_blue,
	output tx_de,
	output tx_hsync,
	output tx_vsync
);

	wire [7:0]r_mux[7:0];	//tablica siedmiu 7-bitowych wektorow
	wire [7:0]g_mux[7:0];	//tablica siedmiu 7-bitowych wektorow
	wire [7:0]b_mux[7:0];	//tablica siedmiu 7-bitowych wektorow

	wire de_mux[7:0];
	wire hsync_mux[7:0];
	wire vsync_mux[7:0];

	assign r_mux[0]     = rx_red;
	assign r_mux[1]     = ycbcr_Y;
	assign r_mux[2]     = bin;

	assign g_mux[0]     = rx_green;
	assign g_mux[1]     = ycbcr_Cb;
	assign g_mux[2]     = bin;

	assign b_mux[0]     = rx_blue;
	assign b_mux[1]     = ycbcr_Cr;
	assign b_mux[2]     = bin;

	assign de_mux[0]    = rx_de;
	assign de_mux[1]    = ycbcr_de;
	assign de_mux[2]    = ycbcr_de;

	assign hsync_mux[0] = rx_hsync;
	assign hsync_mux[1] = ycbcr_hsync;
	assign hsync_mux[2] = ycbcr_hsync;

	assign vsync_mux[0] = rx_vsync;
	assign vsync_mux[1] = ycbcr_vsync;
	assign vsync_mux[2] = ycbcr_vsync;

	/* OUTPUTS assignment */
	assign tx_red   = r_mux[switch_input];
	assign tx_green = g_mux[switch_input];
	assign tx_blue  = b_mux[switch_input];

	assign tx_de    = de_mux[switch_input];
	assign tx_hsync = hsync_mux[switch_input];
	assign tx_vsync = vsync_mux[switch_input];



endmodule


