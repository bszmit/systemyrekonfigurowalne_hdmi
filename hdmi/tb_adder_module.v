`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   10:08:01 04/12/2015
// Design Name:   adder_module
// Module Name:   /home/bszmit/IT/Verilog/SR/rozdz_10/hdmi/tb_adder_module.v
// Project Name:  hdmi
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: adder_module
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_adder_module;

	// Inputs
	reg clk;
	reg ce;
	reg [8:0] a;
	reg [8:0] b;

	// Outputs
	wire [8:0] s;

	// Instantiate the Unit Under Test (UUT)
	adder_module uut (
		.clk(clk), 
		.ce(ce), 
		.a(a), 
		.b(b), 
		.s(s)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		ce = 0;
		a = 0;
		b = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

