`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:07:26 04/22/2015 
// Design Name: 
// Module Name:    centroid 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module centroid
#( parameter IMG_H = 720,
	parameter IMG_W = 576 )
(
	input clk,
	input ce,
	input rst,
	input de,	//poprawnosc piksela
	input hsync,
	input vsync,
	input mask,
	
	output [$clog2(IMG_H)-1:0]x,
	output [$clog2(IMG_W)-1:0]y
   );
	
	reg [9:0]counterHoriz = 0;
	reg [9:0]counterVertic = 0;
	
	reg prev_vsync = 0;

	always @(posedge clk) 
	begin
		prev_vsync <= vsync;
		if (vsync == 0)
		begin
			counterHoriz  <= 0;
			counterVertic <= 0;
		end
		else
		begin
			if (de == 1)
			begin
				counterHoriz <= counterHoriz + 1;
				if (counterHoriz == IMG_W - 1)
				begin
					counterHoriz <= 0;
					counterVertic <= counterVertic + 1;
					if (counterVertic == IMG_H - 1)
					begin
						counterVertic <= 0;
					end
				end
			end
		end
	end	
				
					
	/* eof assign */
	wire eof;
	assign eof = (prev_vsync==1'b1 & vsync==1'b0) 
					? 1'b1 : 1'b0;
	/* eof assign ends */

	/* eof delay */
	wire eof_d;
	delay_line #( .N(1), .DELAY(1) )
	delay_eof (
		.clk(clk),
		.ce(ce),
		.rst(1'b0),
		.idata(eof),
		.odata(eof_d)
	);
	/* eof delay ends */

	/* mask delay */
	wire mask_d;
	delay_line #( .N(1), .DELAY(1) )
	delay_mask(
		.clk(clk),
		.ce(ce),
		.rst(1'b0),
		.idata(mask),
		.odata(mask_d)
	);
	/* mask delay ends*/



	/* m00_sum */
	wire [27:0] m00;
	zad_7_3_module #( .inputWidth(10),
					  .outputWidth(28) )
	m00_sum_module
	(
		.ce(de),
		.clk(clk),
		.rst(eof_d),
		.A({9'b0,mask_d}),

		.Y(m00)
	);
	/* m00_sum ends*/

	/* m01_sum */
	wire [9:0]counterVerticWire;
	assign counterVerticWire = (mask_d == 1) ? counterVertic : 0;
	wire [27:0]m01_sum;

	zad_7_3_module #( .inputWidth(10),
					  .outputWidth(28) )
	m01_sum_module
	(
		.ce(de),
		.clk(clk),
		.rst(eof_d),
		.A(counterVerticWire),

		.Y(m01_sum)
	);
	/* m01_sum ends */


	/* m10_sum */
	wire [27:0]m10_sum;
	wire [9:0]counterHorizWire;
	assign counterHorizWire = (mask_d == 1) 
					? counterHoriz : 0;
	zad_7_3_module #( .inputWidth(10),
							.outputWidth(28) )
	m10_sum_module
	(
		.ce(de),
		.clk(clk),
		.rst(eof_d),
		.A(counterHorizWire),

		.Y(m10_sum)
	);
	/* m10_sum ends */


	/* DIVISION m10/m00 */
	wire readyFlag_x;
	wire [27:0] q_x;
	// Instantiate the module
	divider_28_20 div_x (
		.clk(clk),
		.start(eof_d),
		.dividend(m01_sum), //[27:0]
		.divisor(m00), //[19:0],
		.quotient(q_x), //[27:0]
		.qv(readyFlag_x)
    );
	/* DIVISION m10/m00 ends */
		
	/* DIVISION m01/m00 */
	wire readyFlag_y;
	wire [27:0] q_y;
	// Instantiate the module
	divider_28_20 div_y (
		.clk(clk),
		.start(eof_d),
		.dividend(m10_sum), //[27:0]
		.divisor(m00), //[19:0],
		.quotient(q_y), //[27:0]
		.qv(readyFlag_y)
    );
	/* DIVISION m01/m00 ends */

	/* latch x_reg, y_reg positions */
	reg [27:0] x_reg = 0;
	reg [27:0] y_reg = 0;
	always @(posedge clk)
	begin
		if (readyFlag_x == 1)
		begin
			x_reg <= q_x;
		end

		if (readyFlag_y == 1)
		begin
			y_reg <= q_y;
		end
	end
	/* latch x_reg, y_reg positions ends */

	/* assign output */
	assign x = x_reg[$clog2(IMG_H)-1:0];
	assign y = y_reg[$clog2(IMG_W)-1:0];
	/* assign output */
			

endmodule
