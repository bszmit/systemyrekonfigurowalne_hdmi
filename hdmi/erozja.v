`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:57:56 05/21/2015 
// Design Name: 
// Module Name:    erozja 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module erozja #( parameter H_SIZE = 83 )
(
	input clk,
	input ce,
	input rst,
	input de,
	input hsync,
	input vsync,
	input mask,	//mask is last bit of input 8bit mask

	output [7:0] tx_mask,
	output tx_de,
	output tx_hsync,
	output tx_vsync
    );

		
	wire [3:0] D15_del;
	wire [3:0] D25_del;
	wire [3:0] D35_del;
	wire [3:0] D45_del;

	/*****************************************/
	/**************    ROW 1    **************/
	/*****************************************/
	reg [3:0] D11;
	reg [3:0] D12;
	reg [3:0] D13;
	reg [3:0] D14;
	reg [3:0] D15;

	reg [3:0] D21;
	reg [3:0] D22;
	reg [3:0] D23;
	reg [3:0] D24;
	reg [3:0] D25;

	reg [3:0] D31;
	reg [3:0] D32;
	reg [3:0] D33;
	reg [3:0] D34;
	reg [3:0] D35;
	
	reg [3:0] D41;
	reg [3:0] D42;
	reg [3:0] D43;
	reg [3:0] D44;
	reg [3:0] D45;

	reg [3:0] D51;
	reg [3:0] D52;
	reg [3:0] D53;
	reg [3:0] D54;
	reg [3:0] D55;

	always @(posedge clk) 
	begin
		D11 <= {mask, de, hsync, vsync};
		D12 <= D11;
		D13 <= D12;
		D14 <= D13;
		D15 <= D14;

		D21 <= D15_del;
		D22 <= D21;
		D23 <= D22;
		D24 <= D23;
		D25 <= D24;

		D31 <= D25_del;
		D32 <= D31;
		D33 <= D32;
		D34 <= D33;
		D35 <= D34;

		D41 <= D35_del;
		D42 <= D41;
		D43 <= D42;
		D44 <= D43;
		D45 <= D44;

		D51 <= D45_del;
		D52 <= D51;
		D53 <= D52;
		D54 <= D53;
		D55 <= D54;

	end

	/********        Long Delay        ********/
	/********    D15, D25, D35, D45    ********/
	delayLinieBRAM_WP del (
		.clk(clk),
		.rst(1'b0),
		.ce(1'b1),
		.din({D15, D25, D35, D45}),
		.dout({D15_del, D25_del, D35_del, D45_del}),
		.h_size(H_SIZE-5)
	);


	/********  de delay *********/
	delay_line #(.N(1), .DELAY(2)) 
	delay_vsync (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D33[2]),
		.odata(tx_de)
	);

	assign tx_hsync = D33[1];
	assign tx_vsync = D33[0];
	assign tx_mask = D11[3] && D12[3] && D13[3] && D14[3] && D15[3] && 
					 D21[3] && D22[3] && D23[3] && D24[3] && D25[3] && 
					 D31[3] && D32[3] && D33[3] && D34[3] && D35[3] && 
					 D41[3] && D42[3] && D43[3] && D44[3] && D45[3] && 
					 D51[3] && D52[3] && D53[3] && D54[3] && D55[3] 
					 ? 255 : 0;

endmodule
