`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:58:53 05/21/2015 
// Design Name: 
// Module Name:    zamkniecie_morf 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module zamkniecie_morf #( parameter H_SIZE = 83 ) (
	input clk,
	input ce,
	input rst,
	input de,
	input hsync,
	input vsync,
	input mask,	//mask is last bit of input 8bit mask

	output [7:0] tx_mask,
	output tx_de,
	output tx_hsync,
	output tx_vsync
    );

	wire [7:0] dylatacja_mask;
	wire dylatacja_de;
	wire dylatacja_hsync;
	wire dylatacja_vsync;
	dylatacja #( .H_SIZE(H_SIZE) )
	dyl (
		.clk(clk),
		.ce(ce),
		.de(de),
		.hsync(hsync),
		.vsync(vsync),
		.mask(mask),

		.tx_mask(dylatacja_mask),
		.tx_de(dylatacja_de),
		.tx_hsync(dylatacja_hsync),
		.tx_vsync(dylatacja_vsync)
	);
	
		
	erozja #( .H_SIZE(H_SIZE) )
	eroz (
		.clk(clk),
		.ce(ce),
		.de(de),
		.hsync(dylatacja_hsync),
		.vsync(dylatacja_vsync),
		.mask(dylatacja_mask),

		.tx_mask(tx_mask),
		.tx_de(tx_de),
		.tx_hsync(tx_hsync),
		.tx_vsync(tx_vsync)
	);



endmodule
