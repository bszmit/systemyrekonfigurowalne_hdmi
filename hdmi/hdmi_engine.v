`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    00:05:55 04/29/2015 
// Design Name: 
// Module Name:    hdmi_engine 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module multiplekser (
	input [3:0] switch_input,

	input [7:0] rx_red,
	input [7:0] rx_green,
	input [7:0] rx_blue,

	input [7:0] ycbcr_Y,
	input [7:0] ycbcr_Cb,
	input [7:0] ycbcr_Cr,

	input [7:0] bin,

	input [7:0] centroid_lines_red,
	input [7:0] centroid_lines_green,
	input [7:0] centroid_lines_blue,

	input [7:0] centroid_circle_red,
	input [7:0] centroid_circle_green,
	input [7:0] centroid_circle_blue,

	input [7:0] median,
	input [7:0] erosion,
	input [7:0] dilation,
	input [7:0] opening,
	input [7:0] closing,

	input rx_de,
	input rx_hsync,
	input rx_vsync,

	input ycbcr_de,
	input ycbcr_hsync,
	input ycbcr_vsync,


	input median_de,
	input median_hsync,
	input median_vsync,

	input erosion_de,
	input erosion_hsync,
	input erosion_vsync,

	input dilation_de,
	input dilation_hsync,
	input dilation_vsync,

	input opening_de,
	input opening_hsync,
	input opening_vsync,

	input closing_de,
	input closing_hsync,
	input closing_vsync,

	output [7:0] tx_red,
	output [7:0] tx_green,
	output [7:0] tx_blue,
	output tx_de,
	output tx_hsync,
	output tx_vsync
);


	wire [7:0]r_mux[15:0];	//tablica siedmiu 7-bitowych wektorow
	wire [7:0]g_mux[15:0];	//tablica siedmiu 7-bitowych wektorow
	wire [7:0]b_mux[15:0];	//tablica siedmiu 7-bitowych wektorow

	wire de_mux[15:0];
	wire hsync_mux[15:0];
	wire vsync_mux[15:0];

	assign r_mux[0]     = rx_red;
	assign r_mux[1]     = ycbcr_Y;
	assign r_mux[2]     = bin;
	assign r_mux[3]     = centroid_lines_red;
	assign r_mux[4]     = centroid_circle_red;
	assign r_mux[5]     = median;
	assign r_mux[6]		= erosion;
	assign r_mux[7]		= dilation;
	assign r_mux[8]     = opening;
	assign r_mux[9]     = closing;

	assign g_mux[0]     = rx_green;
	assign g_mux[1]     = ycbcr_Cb;
	assign g_mux[2]     = bin;
	assign g_mux[3]     = centroid_lines_green;
	assign g_mux[4]     = centroid_circle_green;
	assign g_mux[5]     = median;
	assign g_mux[6]		= erosion;
	assign g_mux[7]		= dilation;
	assign g_mux[8]     = opening;
	assign g_mux[9]     = closing;

	assign b_mux[0]     = rx_blue;
	assign b_mux[1]     = ycbcr_Cr;
	assign b_mux[2]     = bin;
	assign b_mux[3]     = centroid_lines_blue;
	assign b_mux[4]     = centroid_circle_blue;
	assign b_mux[5]     = median;
	assign b_mux[6]		= erosion;
	assign b_mux[7]		= dilation;
	assign b_mux[8]     = opening;
	assign b_mux[9]     = closing;

	assign de_mux[0]    = rx_de;
	assign de_mux[1]    = ycbcr_de;
	assign de_mux[2]    = ycbcr_de;
	assign de_mux[3]    = ycbcr_de;
	assign de_mux[4]    = ycbcr_de;
	assign de_mux[5]    = median_de;
	assign de_mux[6]	= erosion_de;
	assign de_mux[7]	= dilation_de;
	assign de_mux[8]    = opening_de;
	assign de_mux[9]    = closing_de;

	assign hsync_mux[0] = rx_hsync;
	assign hsync_mux[1] = ycbcr_hsync;
	assign hsync_mux[2] = ycbcr_hsync;
	assign hsync_mux[3] = ycbcr_hsync;
	assign hsync_mux[4] = ycbcr_hsync;
	assign hsync_mux[5] = median_hsync;
	assign hsync_mux[6]	= erosion_hsync;
	assign hsync_mux[7]	= dilation_hsync;
	assign hsync_mux[8] = opening_hsync;
	assign hsync_mux[9] = closing_hsync;

	assign vsync_mux[0] = rx_vsync;
	assign vsync_mux[1] = ycbcr_vsync;
	assign vsync_mux[2] = ycbcr_vsync;
	assign vsync_mux[3] = ycbcr_vsync;
	assign vsync_mux[4] = ycbcr_vsync;
	assign vsync_mux[5] = median_vsync;
	assign vsync_mux[6]	= erosion_vsync;
	assign vsync_mux[7]	= dilation_vsync;
	assign vsync_mux[8] = opening_vsync;
	assign vsync_mux[9] = closing_vsync;

	/* OUTPUTS assignment */
	assign tx_red   = r_mux[switch_input];
	assign tx_green = g_mux[switch_input];
	assign tx_blue  = b_mux[switch_input];

	assign tx_de    = de_mux[switch_input];
	assign tx_hsync = hsync_mux[switch_input];
	assign tx_vsync = vsync_mux[switch_input];



endmodule


module hdmi_engine(
	input clk,
	input [3:0] switch_input,
	input [7:0] rx_red,
	input [7:0] rx_green,
	input [7:0] rx_blue,
	input rx_de,
	input rx_hsync,
	input rx_vsync,

	output [7:0] tx_red,
	output [7:0] tx_green,
	output [7:0] tx_blue,
	output tx_de,
	output tx_hsync,
	output tx_vsync
);

	localparam IMG_H = 64;	//720
	localparam IMG_W = 64;	//576
	//localparam IMG_H = 720;	//720
	//localparam IMG_W = 576;	//576

	localparam H_SIZE = 83;
	//localparam H_SIZE = 864;


	/******************************************/
	/****************** YCbCr *****************/
	/******************************************/
	wire [7:0] ycbcr_Y;
	wire [7:0] ycbcr_Cb;
	wire [7:0] ycbcr_Cr;
	wire ycbcr_de;
	wire ycbcr_hsync;
	wire ycbcr_vsync;
	rgb2ycbcr rgb2ycbcr_mod(
		.clk(clk),

		.de_in(rx_de),
		.hsync_in(rx_hsync),
		.vsync_in(rx_vsync),

		.R(rx_red),				//without sign
		.G(rx_green),				//without sign			
		.B(rx_blue),				//without sign

		.de_out(ycbcr_de),
		.hsync_out(ycbcr_hsync),
		.vsync_out(ycbcr_vsync),

		.Y(ycbcr_Y),				//without sign
		.Cb(ycbcr_Cb),				//without sign
		.Cr(ycbcr_Cr)				//without sign
	);
	/******************************************/
	/*************** ENDS YCbCr ***************/
	/******************************************/

	

	/******************************************/
	/************* binaryzacja ****************/
	/******************************************/
	localparam Ta = 77;
	localparam Tb = 127;
	localparam Tc = 133;
	localparam Td = 173;
	wire [7:0] bin;
	assign bin = ( ycbcr_Cb > Ta && ycbcr_Cb < Tb 
	&& ycbcr_Cr > Tc && ycbcr_Cr < Td ) ? 8'd255 : 0;
	/******************************************/
	/*********** ENDS binaryzacja *************/
	/******************************************/



	/******************************************/
	/************ centroid lines **************/
	/******************************************/
	wire [7:0]centroid_lines_red;
	wire [7:0]centroid_lines_green;
	wire [7:0]centroid_lines_blue;
	centroid_lines #( .IMG_H(IMG_H), .IMG_W(IMG_W) )
	cent_lines (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.de(ycbcr_de),
		.hsync(ycbcr_hsync),
		.vsync(ycbcr_vsync),
		.bin(bin),

		.tx_red(centroid_lines_red),
		.tx_green(centroid_lines_green),
		.tx_blue(centroid_lines_blue)
	);
	/******************************************/
	/********** ENDS centroid lines ***********/
	/******************************************/


	
	/******************************************/
	/************ centroid circle *************/
	/******************************************/
	wire [7:0]centroid_circle_red;
	wire [7:0]centroid_circle_green;
	wire [7:0]centroid_circle_blue;
	centroid_circle #( .IMG_H(IMG_H), .IMG_W(IMG_W) )
	cent_circle (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.de(ycbcr_de),
		.hsync(ycbcr_hsync),
		.vsync(ycbcr_vsync),
		.bin(bin),

		.tx_red(centroid_circle_red),
		.tx_green(centroid_circle_green),
		.tx_blue(centroid_circle_blue)
	);
	/******************************************/
	/********** ENDS centroid circle **********/
	/******************************************/



	/******************************************/
	/*************** MEDIAN 5x5 ***************/
	/******************************************/
	/*
	wire [7:0] median;
	wire median_de;
	wire median_hsync;
	wire median_vsync;
	wire median_in;
	assign median_in = bin[0];
	median5x5 #( .H_SIZE(H_SIZE) ) 
	med (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.de(ycbcr_de),
		.hsync(ycbcr_hsync),
		.vsync(ycbcr_vsync),
		.mask(median_in),

		.tx_mask(median),
		.tx_de(median_de),
		.tx_hsync(median_hsync),
		.tx_vsync(median_vsync)
	);
	/******************************************/
	/************ ENDS MEDIAN 5x5 *************/
	/******************************************/



	/******************************************/
	/*********** MEDIAN REGISTER 5x5 **********/
	/******************************************/
	wire [7:0] median;
	wire median_de;
	wire median_hsync;
	wire median_vsync;
	wire median_in;
	assign median_in = bin[0];
	median_registers #( .H_SIZE(H_SIZE) ) 
	med_reg (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.de(ycbcr_de),
		.hsync(ycbcr_hsync),
		.vsync(ycbcr_vsync),
		.mask(median_in),

		.tx_mask(median),
		.tx_de(median_de),
		.tx_hsync(median_hsync),
		.tx_vsync(median_vsync)
	);
	/******************************************/
	/******** ENDS MEDIAN REGISTER 5x5 ********/
	/******************************************/



	/******************************************/
	/*************** EROZJA 5x5 ***************/
	/******************************************/
	wire [7:0] erozja_mask;
	wire erozja_de;
	wire erozja_hsync;
	wire erozja_vsync;
	erozja #( .H_SIZE(H_SIZE) )
	eroz (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.de(ycbcr_de),
		.hsync(ycbcr_hsync),
		.vsync(ycbcr_vsync),
		.mask(bin[0]),


		.tx_mask(erozja_mask),
		.tx_de(erozja_de),
		.tx_hsync(erozja_hsync),
		.tx_vsync(erozja_vsync)
	);
	/******************************************/
	/************* ENDS EROZJA 5x5 ************/
	/******************************************/


	

	/******************************************/
	/************** DYLATACJA 5x5 *************/
	/******************************************/
	wire [7:0] dylatacja_mask;
	wire dylatacja_de;
	wire dylatacja_hsync;
	wire dylatacja_vsync;
	dylatacja #( .H_SIZE(83) )	//
	dyl ( 
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.de(ycbcr_de),
		.hsync(ycbcr_hsync),
		.vsync(ycbcr_vsync),
		.mask(bin[0]),

		.tx_mask(dylatacja_mask),
		.tx_de(dylatacja_de),
		.tx_hsync(dylatacja_hsync),
		.tx_vsync(dylatacja_vsync)
	);
	/******************************************/
	/************ ENDS DYLATACJA 5x5 **********/
	/******************************************/



	/******************************************/
	/************** OTWARCIE 5x5 **************/
	/******************************************/
	wire [7:0] otwarcie_morf;
	wire otwarcie_morf_de;
	wire otwarcie_morf_hsync;
	wire otwarcie_morf_vsync;
	wire otwarcie_morf_in;
	assign otwarcie_morf_in = bin[0];
	otwarcie_morf #( .H_SIZE(H_SIZE) )
	otw_morf (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.de(ycbcr_de),
		.hsync(ycbcr_hsync),
		.vsync(ycbcr_vsync),
		.mask(otwarcie_morf_in),

		.tx_mask(otwarcie_morf),
		.tx_de(otwarcie_morf_de),
		.tx_hsync(otwarcie_morf_hsync),
		.tx_vsync(otwarcie_morf_vsync)
	);
	/******************************************/
	/************ ENDS OTWARCIE 5x5 ***********/
	/******************************************/



	/******************************************/
	/************* ZAMKNIECIE 5x5 *************/
	/******************************************/
	wire [7:0] zamkniecie_morf;
	wire zamkniecie_morf_de;
	wire zamkniecie_morf_hsync;
	wire zamkniecie_morf_vsync;
	wire zamkniecie_morf_in;
	assign zamkniecie_morf_in = bin[0];
	zamkniecie_morf #( .H_SIZE(H_SIZE) )
	zam_morf (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.de(ycbcr_de),
		.hsync(ycbcr_hsync),
		.vsync(ycbcr_vsync),
		.mask(zamkniecie_morf_in),

		.tx_mask(zamkniecie_morf),
		.tx_de(zamkniecie_morf_de),
		.tx_hsync(zamkniecie_morf_hsync),
		.tx_vsync(zamkniecie_morf_vsync)
	);
	/******************************************/
	/*********** ENDS ZAMKNIECIE 5x5 **********/
	/******************************************/





	/* MULTIPLEKSER */
	multiplekser multi (	
		.switch_input(switch_input),

		.rx_red(rx_red),
		.rx_green(rx_green),
		.rx_blue(rx_blue),

		.ycbcr_Y(ycbcr_Y),
		.ycbcr_Cb(ycbcr_Cb),
		.ycbcr_Cr(ycbcr_Cr),

		.bin(bin),

		.centroid_lines_red(centroid_lines_red),
		.centroid_lines_green(centroid_lines_green),
		.centroid_lines_blue(centroid_lines_blue),

		.centroid_circle_red(centroid_circle_red),
		.centroid_circle_green(centroid_circle_green),
		.centroid_circle_blue(centroid_circle_blue),

		.median(median),
		.erosion(erozja_mask),
		.dilation(dylatacja_mask),
		.opening(otwarcie_morf),
		.closing(zamkniecie_morf),

		.rx_de(rx_de),
		.rx_hsync(rx_hsync),
		.rx_vsync(rx_vsync),

		.ycbcr_de(ycbcr_de),
		.ycbcr_hsync(ycbcr_hsync),
		.ycbcr_vsync(ycbcr_vsync),

		.median_de(median_de),
		.median_hsync(median_hsync),
		.median_vsync(median_vsync),

		.erosion_de(erozja_de),
		.erosion_hsync(erozja_hsync),
		.erosion_vsync(erozja_vsync),

		.dilation_de(dylatacja_de),
		.dilation_hsync(dylatacja_hsync),
		.dilation_vsync(dylatacja_vsync),

		.opening_de(otwarcie_morf_de),
		.opening_hsync(otwarcie_morf_hsync),
		.opening_vsync(otwarcie_morf_vsync),

		.closing_de(zamkniecie_morf_de),
		.closing_hsync(zamkniecie_morf_hsync),
		.closing_vsync(zamkniecie_morf_vsync),

		.tx_red(tx_red),
		.tx_green(tx_green),
		.tx_blue(tx_blue),
		.tx_de(tx_de),
		.tx_hsync(tx_hsync),
		.tx_vsync(tx_vsync)
	);
	/* MULTIPLEKSER ENDS */


endmodule
