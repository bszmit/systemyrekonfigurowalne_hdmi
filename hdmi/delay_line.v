`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:21:05 03/15/2015 
// Design Name: 
// Module Name:    delay_line 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////


//testbench umieszczony na dole pliku. Wymaga odkomentowania
module register
#(
	parameter N = 4
)
(
	input clk,
	input ce,
	input rst,
	input [N-1:0]d,
	output [N-1:0]q
);
reg [N-1:0]val={{N}{1'b0}};

always @(posedge clk)
begin
	if (rst) val<=0;
	else if(ce) val<=d;
	else val<=val;
end

assign q=val;

endmodule


/*************************************************************/
/*************************************************************/
/*************************************************************/
module delay_line
#(
	parameter N = 4,
	parameter DELAY = 5
)
(
	input clk,
	input ce,
	input rst,
	input [N-1:0]idata,
	output [N-1:0]odata
);

wire [N-1:0] tdata [DELAY:0];
assign tdata[0] = idata;

genvar i;
generate
//nie potrzebuje rozpatrywac osobnego przypadku dla DELAY=0.
// warunek 'if' dla DELAY=0 jest realizowany w warunkach petli for 
for (i =0; i < DELAY; i=i+1)
begin
	register #(.N(N))
	register_i(
		.clk(clk),
		.ce(ce),
		.rst(rst),
		.d(tdata[i]),
		.q(tdata[i+1])
	);
end

endgenerate

assign odata=tdata[DELAY];



endmodule






/*
 *  T E S T B E N C H
 */

//Wrzucone w jednym pliku ze wzgledu
//na limit na uplu (max 1 plik do zadania)

//odkomentowac
/*

module simulate_delay_line
(
	output clk
);

reg reg_clk = 1'b0;
initial
begin
	while(1)
	begin
		#1; reg_clk=1'b0;
		#1; reg_clk=1'b1;
	end
end

assign clk = reg_clk;

endmodule 


module tb_delay_line
#(
	parameter N = 6,
	parameter DELAY = 2
);

	// Inputs
	wire clk;
	reg [N-1:0] idata = 6'b110010;

	simulate_delay_line sim_i
	(
		.clk(clk)
	);

	// Outputs
	wire [N-1:0] odata;

	// Instantiate the Unit Under Test (UUT)
	delay_line 
	#(
		.N(N),
		.DELAY(DELAY)
	)
	uut (
		.clk(clk), 
		.idata(idata), 
		.odata(odata)
	);

      
endmodule

*/
