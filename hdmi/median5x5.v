`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:14:18 05/19/2015 
// Design Name: 
// Module Name:    median5x5 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module median5x5 #( parameter H_SIZE = 83 )
(
	input clk,
	input ce,
	input rst,
	input de,
	input hsync,
	input vsync,
	input mask,	//mask is last bit of input 8bit mask

	output [7:0] tx_mask,
	output tx_de,
	output tx_hsync,
	output tx_vsync
    );

		
	wire [3:0] D15_del;
	wire [3:0] D25_del;
	wire [3:0] D35_del;
	wire [3:0] D45_del;

	/*****************************************/
	/**************    ROW 1    **************/
	/*****************************************/
	wire [3:0] D11;
	wire [3:0] D12;
	wire [3:0] D13;
	wire [3:0] D14;
	wire [3:0] D15;


	/********    D11    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D11 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata({mask, de, hsync, vsync}),
		.odata(D11)
	);

	/********    D12    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D12 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D11),
		.odata(D12)
	);

	/********    D13    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D13 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D12),
		.odata(D13)
	);

	/********    D14    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D14 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D13),
		.odata(D14)
	);

	/********    D15    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D15 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D14),
		.odata(D15)
	);

	/*****************************************/
	/**************    ROW 2    **************/
	/*****************************************/
	wire [3:0] D21;
	wire [3:0] D22;
	wire [3:0] D23;
	wire [3:0] D24;
	wire [3:0] D25;


	/********    D21    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D21 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D15_del), //TODO: sygnal
		.odata(D21)
	);

	/********    D22    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D22 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D21),
		.odata(D22)
	);

	/********    D23    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D23 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D22),
		.odata(D23)
	);

	/********    D24    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D24 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D23),
		.odata(D24)
	);

	/********    D25    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D25 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D24),
		.odata(D25)
	);

	/*****************************************/
	/**************    ROW 3    **************/
	/*****************************************/
	wire [3:0] D31;
	wire [3:0] D32;
	wire [3:0] D33;
	wire [3:0] D34;
	wire [3:0] D35;


	/********    D31    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D31 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D25_del), //TODO: sygnal
		.odata(D31)
	);

	/********    D32    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D32 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D31),
		.odata(D32)
	);

	/********    D33    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D33 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D32),
		.odata(D33)
	);

	/********    D34    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D34 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D33),
		.odata(D34)
	);

	/********    D35    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D35 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D34),
		.odata(D35)
	);



	/*****************************************/
	/**************    ROW 4    **************/
	/*****************************************/
	wire [3:0] D41;
	wire [3:0] D42;
	wire [3:0] D43;
	wire [3:0] D44;
	wire [3:0] D45;


	/********    D41    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D41 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D35_del), //TODO: sygnal
		.odata(D41)
	);

	/********    D42    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D42 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D41),
		.odata(D42)
	);

	/********    D43    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D43 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D42),
		.odata(D43)
	);

	/********    D44    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D44 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D43),
		.odata(D44)
	);

	/********    D45    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D45 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D44),
		.odata(D45)
	);


	/*****************************************/
	/**************    ROW 5    **************/
	/*****************************************/
	wire [3:0] D51;
	wire [3:0] D52;
	wire [3:0] D53;
	wire [3:0] D54;
	wire [3:0] D55;


	/********    D51    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D51 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D45_del), //TODO: sygnal
		.odata(D51)
	);

	/********    D52    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D52 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D51),
		.odata(D52)
	);

	/********    D53    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D53 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D52),
		.odata(D53)
	);

	/********    D54    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D54 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D53),
		.odata(D54)
	);

	/********    D55    ********/
	delay_line #(.N(4), .DELAY(1)) 
	delay_D55 (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D54),
		.odata(D55)
	);


	
	/********        Long Delay        ********/
	/********    D15, D25, D35, D45    ********/
	delayLinieBRAM_WP del (
		.clk(clk),
		.rst(1'b0),
		.ce(1'b1),
		.din({D15, D25, D35, D45}),
		.dout({D15_del, D25_del, D35_del, D45_del}),
		.h_size(H_SIZE-5)
	);


	reg [2:0] ABCDE = 0;
	reg [2:0] FGHIJ = 0;
	reg [2:0] KLMNO = 0;
	reg [2:0] PQRST = 0;
	reg [2:0] UWXYZ = 0;

	wire context_valid;
	assign context_valid = D11[2] & D12[2] & D13[2] & D14[2] & D15[2] &
						   D21[2] & D22[2] & D23[2] & D24[2] & D25[2] &
						   D31[2] & D32[2] & D33[2] & D34[2] & D35[2] &
						   D41[2] & D42[2] & D43[2] & D44[2] & D45[2] &
						   D51[2] & D52[2] & D53[2] & D54[2] & D55[2];
	reg [4:0] sum = 0;
	always @(posedge clk) 
	begin
		ABCDE <= D11[3] + D12[3] + D13[3] + D14[3] + D15[3];
		FGHIJ <= D21[3] + D22[3] + D23[3] + D24[3] + D25[3];
		KLMNO <= D31[3] + D32[3] + D33[3] + D34[3] + D35[3];
		PQRST <= D41[3] + D42[3] + D43[3] + D44[3] + D45[3];
		UWXYZ <= D51[3] + D52[3] + D53[3] + D54[3] + D55[3];
		sum <= ABCDE + FGHIJ + KLMNO + PQRST + UWXYZ;
	end

	wire context_valid_delay;
	/********  context_valid delay  ********/
	delay_line #(.N(1), .DELAY(2)) 
	delay_context_valid (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(context_valid),
		.odata(context_valid_delay)
	);

	/********  de delay *********/
	delay_line #(.N(1), .DELAY(2)) 
	delay_vsync (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(D33[2]),
		.odata(tx_de)
	);

	assign tx_hsync = D33[1];
	assign tx_vsync = D33[0];
	assign tx_mask = sum > 5'd12 ? 255 : 0;

endmodule
