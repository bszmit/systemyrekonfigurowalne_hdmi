`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   10:09:07 04/12/2015
// Design Name:   multiplier
// Module Name:   /home/bszmit/IT/Verilog/SR/rozdz_10/hdmi/tb_multiplier.v
// Project Name:  hdmi
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: multiplier
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_multiplier;

	// Inputs
	wire clk;
	reg [17:0] a = 18'b010011001000101101;
	reg [17:0] b = 18'b000000000010000000; //{10'b0 + 8bit}

	generate_clock gen_clk (
		.clk(clk)
	);

	// Outputs
	wire [35:0] p;

	// Instantiate the Unit Under Test (UUT)
	multiplier uut (
		.clk(clk), 
		.a(a), 
		.b(b), 
		.p(p)
	);

	wire signed [8:0]result;
	assign result = p[26:18];

	initial begin
		// Initialize Inputs
		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

