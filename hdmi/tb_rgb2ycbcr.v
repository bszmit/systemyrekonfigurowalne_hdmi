`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   10:03:26 04/12/2015
// Design Name:   rgb2ycbcr
// Module Name:   /home/bszmit/IT/Verilog/SR/rozdz_10/hdmi/tb_rgb2ycbcr.v
// Project Name:  hdmi
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: rgb2ycbcr
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module generate_clock (
	output clk
);
reg clk_reg = 1'b0;
assign clk = clk_reg;

initial
begin
	while(1)
	begin
		#1; clk_reg = 1'b0;
		#1; clk_reg = 1'b1;
	end
end
endmodule

module tb_rgb2ycbcr;

	// Inputs
	wire clk;
	reg de_in;// = 1'b0;
	reg hsync_in;// = 1'b0;
	reg vsync_in;// = 1'b0;
	reg [7:0] R;// = 8'b01001001;  //73
	reg [7:0] G;// = 8'b11100111;	//231
	reg [7:0] B;// = 0'b00000100;	//4

	generate_clock gen_clk (
		.clk(clk)
	);

	// Outputs
	wire de_out;
	wire hsync_out;
	wire vsync_out;
	wire [7:0] Y;
	wire [7:0] Cb;
	wire [7:0] Cr;



	// Instantiate the Unit Under Test (UUT)
	rgb2ycbcr uut (
		.clk(clk), 
		.de_in(de_in), 
		.hsync_in(hsync_in), 
		.vsync_in(vsync_in), 
		.R(R), 
		.G(G), 
		.B(B), 
		.de_out(de_out), 
		.hsync_out(hsync_out), 
		.vsync_out(vsync_out), 
		.Y(Y), 
		.Cb(Cb), 
		.Cr(Cr)
	);

	initial begin
		// Initialize Inputs
		// Wait 100 ns for global reset to finish
		#100;
		de_in = 1'b0;
		hsync_in = 1'b0;
		vsync_in = 1'b0;
		R = 8'b11111111;  //73
		G = 8'b11111111;	//231
		B = 0'b11111111;	//4
		#8;
		de_in = 1'b1;
		hsync_in = 1'b1;
		vsync_in = 1'b1;
		R = 8'b01011001;  //73
		G = 8'b11001111;	//231
		B = 0'b00110100;	//4
        
		// Add stimulus here

	end
      
endmodule

