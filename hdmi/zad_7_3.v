`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    00:51:45 04/01/2015 
// Design Name: 
// Module Name:    zad_7_3 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module zad_7_3
#(
	parameter inputWidth = 2,
	parameter outputWidth = 2
)(
	input clk,
	input rst,
	input ce,
	input [inputWidth-1:0]A,

	output [outputWidth-1:0]Y
    );

	wire signed [outputWidth-1:0]B;	//goes to register

	//latency 0
	sumator_module sumator(
		.a(A), // input [9 : 0] a
		.b(Y), // input [27 : 0] b
		.s(B) // output [27 : 0] s
	);


	delay_line #(
		.N(outputWidth),  //z21c4u
		.DELAY(1)
	) akumulator (
		.clk(clk),
		.ce(ce),
		.rst(rst),
		.idata(B),
		.odata(Y)
	);


endmodule



/* T E S T B E N C H */
/*
module generateClock (
	output clk
);
reg reg_clk = 1'b0;

initial
begin
	while(1)
	begin
		#1; reg_clk = 1'b0;
		#1; reg_clk = 1'b1;
	end
end

assign clk = reg_clk;
endmodule


module tb_zad_7_3;

	// Inputs
	wire clk;
	reg rst = 1'b0;
	reg ce = 1'b1;
	reg [12:0] A = 13'b0000000000001;

	generateClock genClk (
		.clk(clk)
	);

	// Outputs
	wire [255:0] Y;

	// Instantiate the Unit Under Test (UUT)
	zad_7_3 uut (
		.clk(clk), 
		.rst(rst), 
		.ce(ce), 
		.A(A), 
		.Y(Y)
	);

	initial begin
		// Initialize Inputs
		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule
*/
