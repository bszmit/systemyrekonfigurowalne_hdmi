`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:55:20 05/06/2015 
// Design Name: 
// Module Name:    zad_7_3_module 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module zad_7_3_module
#(  parameter inputWidth = 10,
	parameter outputWidth = 28)
(
	input ce,
	input clk,
	input rst,
	input [inputWidth-1:0]A,

	output [outputWidth-1:0]Y
);
/*
	reg [outputWidth-1:0]reg_Y = 0; 
	assign Y = reg_Y;

	always @(posedge clk) 
	begin
		if (rst == 1)
		begin
			reg_Y <= 0;
		end
	end
	*/

	wire [outputWidth-1:0]B;
	//latency = 0	
	sumator_module_7_3 sumator(
		.a(A), // input [9 : 0] a
		.b(Y), // input [27 : 0] b
		.s(B) // output [17 : 0] s
	);


	delay_line
	#(
		.N(outputWidth),
		.DELAY(1)
	) delay1
	(
		.clk(clk),
		.ce(ce),
		.rst(rst),
		.idata(B),
		.odata(Y)
	);




endmodule
