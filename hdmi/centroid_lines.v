`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:56:55 05/07/2015 
// Design Name: 
// Module Name:    centroid_lines 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////


module centroid_lines
#(	parameter IMG_H = 720,
	parameter IMG_W = 576 )
(
	input clk,
	input ce,
	input rst,
	input de,
	input hsync, 
	input vsync,
	input [7:0]bin,

	output [7:0] tx_red,
	output [7:0] tx_green,
	output [7:0] tx_blue
);
	wire bin_unary;
	assign bin_unary = (bin == 0) ? 0 : 1;

	wire [$clog2(IMG_H)-1:0]x_pos;
	wire [$clog2(IMG_W)-1:0]y_pos;
	centroid #( .IMG_H(IMG_H), .IMG_W(IMG_W) )
	centr (
		.clk(clk),
		.ce(ce),
		.rst(rst),
		.de(de),
		.hsync(hsync),
		.vsync(vsync),
		.mask(bin_unary),

		.x(x_pos),
		.y(y_pos)
	);

	reg prev_vsync = 0;
	reg [9:0]x_cnt = 0;
	reg [9:0]y_cnt = 0;

	always @(posedge clk) 
	begin
		prev_vsync <= vsync;
		if (vsync == 0)
		begin
			x_cnt <= 0;
			y_cnt <= 0;
		end
		else
		begin
			if (de == 1)
			begin
				y_cnt <= y_cnt + 1;
				if (y_cnt == IMG_W - 1)
				begin
					y_cnt <= 0;
					x_cnt <= x_cnt + 1;
					if (x_cnt == IMG_H - 1)
					begin
						x_cnt <= 0;
					end
				end
			end
		end
	end	

	wire [7:0]tmp_red;
	wire [7:0]tmp_green;
	wire [7:0]tmp_blue;
	assign tmp_red   = ((x_cnt==x_pos || y_cnt==y_pos) ? 8'hff : bin);
	assign tmp_green = ((x_cnt==x_pos || y_cnt==y_pos) ? 8'h00 : bin);
	assign tmp_blue  = ((x_cnt==x_pos || y_cnt==y_pos) ? 8'h00 : bin);
	/*assign tx_red   = ((x_cnt==x_pos || y_cnt==y_pos) ? 8'hff : bin);
	assign tx_green = ((x_cnt==x_pos || y_cnt==y_pos) ? 8'h00 : bin);
	assign tx_blue  = ((x_cnt==x_pos || y_cnt==y_pos) ? 8'h00 : bin);
	*/

	assign tx_green = ((x_cnt == x_pos + 5 && y_cnt == y_pos + 0) || 
				       (x_cnt == x_pos + 5 && y_cnt == y_pos + 1) ||
				       (x_cnt == x_pos + 5 && y_cnt == y_pos + 2) ||
				       (x_cnt == x_pos + 4 && y_cnt == y_pos + 2) ||
				       (x_cnt == x_pos + 4 && y_cnt == y_pos + 3) ||
				       (x_cnt == x_pos + 3 && y_cnt == y_pos + 3) ||
				       (x_cnt == x_pos + 3 && y_cnt == y_pos + 4) ||
				       (x_cnt == x_pos + 2 && y_cnt == y_pos + 4) ||
				       (x_cnt == x_pos + 2 && y_cnt == y_pos + 5) ||
				       (x_cnt == x_pos + 1 && y_cnt == y_pos + 5) ||
				       (x_cnt == x_pos + 0 && y_cnt == y_pos + 5) ||
				       (x_cnt == x_pos - 1 && y_cnt == y_pos + 5) ||
				       (x_cnt == x_pos - 2 && y_cnt == y_pos + 5) ||
				       (x_cnt == x_pos - 2 && y_cnt == y_pos + 4) ||
				       (x_cnt == x_pos - 3 && y_cnt == y_pos + 4) ||
				       (x_cnt == x_pos - 3 && y_cnt == y_pos + 3) ||
				       (x_cnt == x_pos - 4 && y_cnt == y_pos + 3) ||
				       (x_cnt == x_pos - 4 && y_cnt == y_pos + 2) ||
				       (x_cnt == x_pos - 5 && y_cnt == y_pos + 2) ||
				       (x_cnt == x_pos - 5 && y_cnt == y_pos + 1) ||
				       (x_cnt == x_pos - 5 && y_cnt == y_pos + 0) ||
				       (x_cnt == x_pos - 5 && y_cnt == y_pos - 1) ||
				       (x_cnt == x_pos - 5 && y_cnt == y_pos - 2) ||
				       (x_cnt == x_pos - 4 && y_cnt == y_pos - 2) ||
				       (x_cnt == x_pos - 4 && y_cnt == y_pos - 3) ||
				       (x_cnt == x_pos - 3 && y_cnt == y_pos - 3) ||
				       (x_cnt == x_pos - 3 && y_cnt == y_pos - 4) ||
				       (x_cnt == x_pos - 2 && y_cnt == y_pos - 4) ||
				       (x_cnt == x_pos - 2 && y_cnt == y_pos - 5) ||
				       (x_cnt == x_pos - 1 && y_cnt == y_pos - 5) ||
				       (x_cnt == x_pos + 0 && y_cnt == y_pos - 5) ||
				       (x_cnt == x_pos + 1 && y_cnt == y_pos - 5) ||
				       (x_cnt == x_pos + 2 && y_cnt == y_pos - 5) ||
				       (x_cnt == x_pos + 2 && y_cnt == y_pos - 4) ||
				       (x_cnt == x_pos + 3 && y_cnt == y_pos - 4) ||
				       (x_cnt == x_pos + 3 && y_cnt == y_pos - 3) ||
				       (x_cnt == x_pos + 4 && y_cnt == y_pos - 3) ||
				       (x_cnt == x_pos + 4 && y_cnt == y_pos - 2) ||
				       (x_cnt == x_pos + 5 && y_cnt == y_pos - 2) ||
				       (x_cnt == x_pos + 5 && y_cnt == y_pos - 1) 
					   ? 8'hff : tmp_green);

	assign tx_red = ((x_cnt == x_pos + 5 && y_cnt == y_pos + 0) || 
				       (x_cnt == x_pos + 5 && y_cnt == y_pos + 1) ||
				       (x_cnt == x_pos + 5 && y_cnt == y_pos + 2) ||
				       (x_cnt == x_pos + 4 && y_cnt == y_pos + 2) ||
				       (x_cnt == x_pos + 4 && y_cnt == y_pos + 3) ||
				       (x_cnt == x_pos + 3 && y_cnt == y_pos + 3) ||
				       (x_cnt == x_pos + 3 && y_cnt == y_pos + 4) ||
				       (x_cnt == x_pos + 2 && y_cnt == y_pos + 4) ||
				       (x_cnt == x_pos + 2 && y_cnt == y_pos + 5) ||
				       (x_cnt == x_pos + 1 && y_cnt == y_pos + 5) ||
				       (x_cnt == x_pos + 0 && y_cnt == y_pos + 5) ||
				       (x_cnt == x_pos - 1 && y_cnt == y_pos + 5) ||
				       (x_cnt == x_pos - 2 && y_cnt == y_pos + 5) ||
				       (x_cnt == x_pos - 2 && y_cnt == y_pos + 4) ||
				       (x_cnt == x_pos - 3 && y_cnt == y_pos + 4) ||
				       (x_cnt == x_pos - 3 && y_cnt == y_pos + 3) ||
				       (x_cnt == x_pos - 4 && y_cnt == y_pos + 3) ||
				       (x_cnt == x_pos - 4 && y_cnt == y_pos + 2) ||
				       (x_cnt == x_pos - 5 && y_cnt == y_pos + 2) ||
				       (x_cnt == x_pos - 5 && y_cnt == y_pos + 1) ||
				       (x_cnt == x_pos - 5 && y_cnt == y_pos + 0) ||
				       (x_cnt == x_pos - 5 && y_cnt == y_pos - 1) ||
				       (x_cnt == x_pos - 5 && y_cnt == y_pos - 2) ||
				       (x_cnt == x_pos - 4 && y_cnt == y_pos - 2) ||
				       (x_cnt == x_pos - 4 && y_cnt == y_pos - 3) ||
				       (x_cnt == x_pos - 3 && y_cnt == y_pos - 3) ||
				       (x_cnt == x_pos - 3 && y_cnt == y_pos - 4) ||
				       (x_cnt == x_pos - 2 && y_cnt == y_pos - 4) ||
				       (x_cnt == x_pos - 2 && y_cnt == y_pos - 5) ||
				       (x_cnt == x_pos - 1 && y_cnt == y_pos - 5) ||
				       (x_cnt == x_pos + 0 && y_cnt == y_pos - 5) ||
				       (x_cnt == x_pos + 1 && y_cnt == y_pos - 5) ||
				       (x_cnt == x_pos + 2 && y_cnt == y_pos - 5) ||
				       (x_cnt == x_pos + 2 && y_cnt == y_pos - 4) ||
				       (x_cnt == x_pos + 3 && y_cnt == y_pos - 4) ||
				       (x_cnt == x_pos + 3 && y_cnt == y_pos - 3) ||
				       (x_cnt == x_pos + 4 && y_cnt == y_pos - 3) ||
				       (x_cnt == x_pos + 4 && y_cnt == y_pos - 2) ||
				       (x_cnt == x_pos + 5 && y_cnt == y_pos - 2) ||
				       (x_cnt == x_pos + 5 && y_cnt == y_pos - 1) 
					   ? 8'h00 : tmp_red);

	assign tx_blue  = ((x_cnt == x_pos + 5 && y_cnt == y_pos + 0) || 
				       (x_cnt == x_pos + 5 && y_cnt == y_pos + 1) ||
				       (x_cnt == x_pos + 5 && y_cnt == y_pos + 2) ||
				       (x_cnt == x_pos + 4 && y_cnt == y_pos + 2) ||
				       (x_cnt == x_pos + 4 && y_cnt == y_pos + 3) ||
				       (x_cnt == x_pos + 3 && y_cnt == y_pos + 3) ||
				       (x_cnt == x_pos + 3 && y_cnt == y_pos + 4) ||
				       (x_cnt == x_pos + 2 && y_cnt == y_pos + 4) ||
				       (x_cnt == x_pos + 2 && y_cnt == y_pos + 5) ||
				       (x_cnt == x_pos + 1 && y_cnt == y_pos + 5) ||
				       (x_cnt == x_pos + 0 && y_cnt == y_pos + 5) ||
				       (x_cnt == x_pos - 1 && y_cnt == y_pos + 5) ||
				       (x_cnt == x_pos - 2 && y_cnt == y_pos + 5) ||
				       (x_cnt == x_pos - 2 && y_cnt == y_pos + 4) ||
				       (x_cnt == x_pos - 3 && y_cnt == y_pos + 4) ||
				       (x_cnt == x_pos - 3 && y_cnt == y_pos + 3) ||
				       (x_cnt == x_pos - 4 && y_cnt == y_pos + 3) ||
				       (x_cnt == x_pos - 4 && y_cnt == y_pos + 2) ||
				       (x_cnt == x_pos - 5 && y_cnt == y_pos + 2) ||
				       (x_cnt == x_pos - 5 && y_cnt == y_pos + 1) ||
				       (x_cnt == x_pos - 5 && y_cnt == y_pos + 0) ||
				       (x_cnt == x_pos - 5 && y_cnt == y_pos - 1) ||
				       (x_cnt == x_pos - 5 && y_cnt == y_pos - 2) ||
				       (x_cnt == x_pos - 4 && y_cnt == y_pos - 2) ||
				       (x_cnt == x_pos - 4 && y_cnt == y_pos - 3) ||
				       (x_cnt == x_pos - 3 && y_cnt == y_pos - 3) ||
				       (x_cnt == x_pos - 3 && y_cnt == y_pos - 4) ||
				       (x_cnt == x_pos - 2 && y_cnt == y_pos - 4) ||
				       (x_cnt == x_pos - 2 && y_cnt == y_pos - 5) ||
				       (x_cnt == x_pos - 1 && y_cnt == y_pos - 5) ||
				       (x_cnt == x_pos + 0 && y_cnt == y_pos - 5) ||
				       (x_cnt == x_pos + 1 && y_cnt == y_pos - 5) ||
				       (x_cnt == x_pos + 2 && y_cnt == y_pos - 5) ||
				       (x_cnt == x_pos + 2 && y_cnt == y_pos - 4) ||
				       (x_cnt == x_pos + 3 && y_cnt == y_pos - 4) ||
				       (x_cnt == x_pos + 3 && y_cnt == y_pos - 3) ||
				       (x_cnt == x_pos + 4 && y_cnt == y_pos - 3) ||
				       (x_cnt == x_pos + 4 && y_cnt == y_pos - 2) ||
				       (x_cnt == x_pos + 5 && y_cnt == y_pos - 2) ||
				       (x_cnt == x_pos + 5 && y_cnt == y_pos - 1) 
					   ? 8'h00 : tmp_blue);


endmodule
