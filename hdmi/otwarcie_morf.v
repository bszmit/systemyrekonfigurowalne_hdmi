`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:30:42 05/21/2015 
// Design Name: 
// Module Name:    otwarcie_morf 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module otwarcie_morf #( parameter H_SIZE = 83 )
(
	input clk,
	input ce,
	input rst,
	input de,
	input hsync,
	input vsync,
	input mask,	//mask is last bit of input 8bit mask

	output [7:0] tx_mask,
	output tx_de,
	output tx_hsync,
	output tx_vsync
    );

		
	wire [7:0] erozja_mask;
	wire erozja_de;
	wire erozja_hsync;
	wire erozja_vsync;
	erozja #( .H_SIZE(H_SIZE) )
	eroz (
		.clk(clk),
		.ce(ce),
		.de(de),
		.hsync(hsync),
		.vsync(vsync),
		.mask(mask),

		.tx_mask(erozja_mask),
		.tx_de(erozja_de),
		.tx_hsync(erozja_hsync),
		.tx_vsync(erozja_vsync)
	);

	dylatacja #( .H_SIZE(H_SIZE) )
	dyl (
		.clk(clk),
		.ce(ce),
		.de(erozja_de),
		.hsync(erozja_hsync),
		.vsync(erozja_vsync),
		.mask(erozja_mask),

		.tx_mask(tx_mask),
		.tx_de(tx_de),
		.tx_hsync(tx_hsync),
		.tx_vsync(tx_vsync)
	);
	

endmodule
