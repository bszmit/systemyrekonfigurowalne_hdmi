`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:28:50 05/06/2015
// Design Name:   zad_7_3_module
// Module Name:   /home/lsriw/SR/SzmitBartosz/rozdz_11_2/hdmi/tb_zad_7_3_module.v
// Project Name:  hdmi
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: zad_7_3_module
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module generate_clock_tb (
	output clk
);
reg clk_reg = 1'b0;
assign clk = clk_reg;

initial
begin
	while(1)
	begin
		#1; clk_reg = 1'b0;
		#1; clk_reg = 1'b1;
	end
end
endmodule

module tb_zad_7_3_module;

	// Inputs
	reg ce = 1'b1;
	wire clk;
	reg rst = 1'b0;
	reg [9:0] A = 10'b0;

	// Outputs
	wire [17:0] Y;

	generate_clock_tb gc (
	.clk(clk)
	);
	// Instantiate the Unit Under Test (UUT)
	zad_7_3_module uut (
		.ce(ce), 
		.clk(clk), 
		.rst(rst), 
		.A(A), 
		.Y(Y)
	);

	always @(posedge clk) 
	begin
		A <= A + 1;
	end

	initial begin
		// Initialize Inputs
		

		// Wait 100 ns for global reset to finish
        
		// Add stimulus here

	end
      
endmodule

