`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    00:57:46 04/12/2015 
// Design Name: 
// Module Name:    rgb2ycbcr 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

`define START_CONCAT 25
`define STOP_CONCAT 17
module rgb2ycbcr(
	input clk,

	input de_in,
	input hsync_in,
	input vsync_in,

	input [7:0]R,				//without sign
	input [7:0]G,				//without sign			
	input [7:0]B,				//without sign
	//input [26:0] pixel_in,	//3* (1+8) = 27

	output de_out,
	output hsync_out,
	output vsync_out,

	output [7:0]Y,				//without sign
	output [7:0]Cb,				//without sign
	output [7:0]Cr				//without sign
	//output [26:0] pixel_out	//3* (1+8) = 27

    );

	/**********************************************/
	/* R */
	/**********************************************/
	wire signed [35:0] Rma_tmp;	//R multiply a
	wire signed [8:0] Rma;	//R multiply a
	reg signed [17:0] a = 18'b001001100100010111;	//0.299
	//latency 2 
	multiplier multiply_R_a (
		.clk(clk), 
		.a({10'b0,R}),	//[17:0]
		.b(a),			//[17:0]
		.p(Rma_tmp)		//[35:0]
    );
	assign Rma = Rma_tmp[`START_CONCAT:`STOP_CONCAT];

	wire signed [35:0] Rmd_tmp;	//R multiply d
	wire signed [8:0] Rmd;	//R multiply g
	reg signed [17:0] d = 18'b111010100110011011;	//-0.168736
	//latency 2 
	multiplier multiply_R_d (
		.clk(clk), 
		.a({10'b0,R}),	//[17:0]
		.b(d),			//[17:0]
		.p(Rmd_tmp)		//[35:0]
    );
	assign Rmd = Rmd_tmp[`START_CONCAT:`STOP_CONCAT];

	wire signed [35:0] Rmg_tmp;	//R multiply g
	wire signed [8:0] Rmg;	//R multiply g
	reg signed [17:0] g = 18'b010000000000000000;	//0.5
	//latency 2 
	multiplier multiply_R_g (
		.clk(clk), 
		.a({10'b0,R}),	//[17:0]
		.b(g),			//[17:0]
		.p(Rmg_tmp)		//[35:0]
    );
	assign Rmg = Rmg_tmp[`START_CONCAT:`STOP_CONCAT];


	/**********************************************/
	/* G */
	/**********************************************/
	wire signed [35:0] Gmb_tmp;	//G multiply b
	wire signed [8:0] Gmb;	//G multiply b
	reg signed [17:0] b = 18'b010010110010001011;	//0.587
	//latency 2 
	multiplier multiply_G_b (
		.clk(clk), 
		.a({10'b0,G}),	//[17:0]
		.b(b),			//[17:0]
		.p(Gmb_tmp)		//[35:0]
    );
	assign Gmb = Gmb_tmp[`START_CONCAT:`STOP_CONCAT];

	wire signed [35:0] Gme_tmp;	//G multiply e
	wire signed [8:0] Gme;	//G multiply e
	reg signed [17:0] e = 18'b110101011001100101;	//-0.331264
	//latency 2 
	multiplier multiply_G_e (
		.clk(clk), 
		.a({10'b0,G}),	//[17:0]
		.b(e),			//[17:0]
		.p(Gme_tmp)		//[35:0]
    );
	assign Gme = Gme_tmp[`START_CONCAT:`STOP_CONCAT];

	wire signed [35:0] Gmh_tmp;	//G multiply h
	wire signed [8:0] Gmh;	//G multiply h
	reg signed [17:0] h = 18'b110010100110100010;	//-0.418688
	//latency 2 
	multiplier multiply_G_h (
		.clk(clk), 
		.a({10'b0,G}),	//[17:0]
		.b(h),			//[17:0]
		.p(Gmh_tmp)		//[35:0]
    );
	assign Gmh = Gmh_tmp[`START_CONCAT:`STOP_CONCAT];

	/**********************************************/
	/* B */
	/**********************************************/
	wire signed [35:0] Bmc_tmp;	//B multiply c
	wire signed [8:0] Bmc_noDelay;	//B multiply c not delayed
	wire signed [8:0] Bmc;	//B multiply c delayed (2 latency)
	reg signed [17:0] c = 18'b000011101001011110;	//0.114
	//latency 2 
	multiplier multiply_B_c (
		.clk(clk), 
		.a({10'b0,B}),	//[17:0]
		.b(c),			//[17:0]
		.p(Bmc_tmp)		//[35:0]
    );
	assign Bmc_noDelay = Bmc_tmp[`START_CONCAT:`STOP_CONCAT];

	delay_line #( .N(9), .DELAY(2) ) 
	delay_Bmc (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(Bmc_noDelay),	//[N-1:0]
		.odata(Bmc)				//[N-1:0]
	);

	wire signed [35:0] Bmf_tmp;	//B multiply f
	wire signed [8:0] Bmf_noDelay;	//B multiply f not delayed
	wire signed [8:0] Bmf;	//B multiply f delayed (latncy 2)
	reg signed [17:0] f = 18'b010000000000000000;	//0.5
	//latency 2 
	multiplier multiply_B_f (
		.clk(clk), 
		.a({10'b0,B}),	//[17:0]
		.b(f),			//[17:0]
		.p(Bmf_tmp)		//[35:0]
    );
	assign Bmf_noDelay = Bmf_tmp[`START_CONCAT:`STOP_CONCAT];
	delay_line #( .N(9), .DELAY(2) ) 
	delay_Bmf (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(Bmf_noDelay),	//[N-1:0]
		.odata(Bmf)				//[N-1:0]
	);

	wire signed [35:0] Bmi_tmp;	//B multiply i
	wire signed [8:0] Bmi_noDelay;	//B multiply i not delayed
	wire signed [8:0] Bmi;	//B multiply i delayed (latency 2)
	reg signed [17:0] i = 18'b111101011001011110;	//-0.081312
	//latency 2 
	multiplier multiply_B_i (
		.clk(clk), 
		.a({10'b0,B}),	//[17:0]
		.b(i),			//[17:0]
		.p(Bmi_tmp)		//[35:0]
    );
	assign Bmi_noDelay = Bmi_tmp[`START_CONCAT:`STOP_CONCAT];

	delay_line #( .N(9), .DELAY(2) ) 
	delay_Bmi (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(Bmi_noDelay),	//[N-1:0]
		.odata(Bmi)				//[N-1:0]
	);



	/**********************************************/
	/**********************************************/
	/**********************************************/
	/**********************************************/

	reg reg_ce = 1'b1;
	/**********************************************/
	/* Ra + Gb + Bc */
	/**********************************************/
	wire signed [8:0] RmapGmb;	//Rma + Gmb
	//latency 2
	adder_module add_Rma_p_Gmb (
		.a(Rma), // input [8 : 0] a
		.b(Gmb), // input [8 : 0] b
		.clk(clk), // input clk
		.ce(reg_ce), // input ce
		.s(RmapGmb) // output [8 : 0] s
	);

	wire signed [8:0] RmapGmbpBmc;	//(Rma+Gmb) + Bmc
	//latency 2
	adder_module add_RmapGmb_Bmc (
		.a(RmapGmb), // input [8 : 0] a
		.b(Bmc), // input [8 : 0] b
		.clk(clk), // input clk
		.ce(reg_ce), // input ce
		.s(RmapGmbpBmc) // output [8 : 0] s
	);

	/**********************************************/
	/* Rd + Ge + Bf */
	/**********************************************/
	wire signed [8:0] RmdpGme;	//Rmd + Gme
	//latency 2
	adder_module add_Rmd_p_Gme (
		.a(Rmd), // input [8 : 0] a
		.b(Gme), // input [8 : 0] b
		.clk(clk), // input clk
		.ce(reg_ce), // input ce
		.s(RmdpGme) // output [8 : 0] s
	);

	wire signed [8:0] RmdpGmepBmf;	//(Rmd + Gme) + Bmf 
	//latency 2
	adder_module add_RmdpGme_Bmf (
		.a(RmdpGme), // input [8 : 0] a
		.b(Bmf), // input [8 : 0] b
		.clk(clk), // input clk
		.ce(reg_ce), // input ce
		.s(RmdpGmepBmf) // output [8 : 0] s
	);

	/**********************************************/
	/* Rg + Gh + Bi */
	/**********************************************/
	wire signed [8:0] RmgpGmh;	//Rmg + Gmh
	//latency 2
	adder_module add_Rmg_p_Gmh (
		.a(Rmg), // input [8 : 0] a
		.b(Gmh), // input [8 : 0] b
		.clk(clk), // input clk
		.ce(reg_ce), // input ce
		.s(RmgpGmh) // output [8 : 0] s
	);

	wire signed [8:0] RmgpGmhpBmi;	//(Rmg + Gmh) + Bmi
	//latency 2
	adder_module add_RmgpGmh_Bmi (
		.a(RmgpGmh), // input [8 : 0] a
		.b(Bmi), // input [8 : 0] b
		.clk(clk), // input clk
		.ce(reg_ce), // input ce
		.s(RmgpGmhpBmi) // output [8 : 0] s
	);


	/**********************************************/
	/* Y, Cb, Cr
	/**********************************************/

	reg signed [8:0]rest = 9'b010000000;	//128 


	wire signed [8:0] Y_signed;
	wire [7:0] Y;
	delay_line #( .N(9), .DELAY(2) ) 
	delay_Y (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata(RmapGmbpBmc),	//[N-1:0]
		.odata(Y_signed)				//[N-1:0]
	);
	assign Y = Y_signed[7:0];

	wire signed [8:0] Cb_signed;
	wire [7:0] Cb;
	//latency 2
	adder_module add_Cb(
		.a(RmdpGmepBmf), // input [8 : 0] a
		.b(rest), // input [8 : 0] b
		.clk(clk), // input clk
		.ce(reg_ce), // input ce
		.s(Cb_signed) // output [8 : 0] s
	);
	assign Cb = Cb_signed[7:0];

	wire signed [8:0] Cr_signed;
	wire [7:0] Cr;
	//latency 2
	adder_module add_Cr (
		.a(RmgpGmhpBmi), // input [8 : 0] a
		.b(rest), // input [8 : 0] b
		.clk(clk), // input clk
		.ce(reg_ce), // input ce
		.s(Cr_signed) // output [8 : 0] s
	);
	assign Cr = Cr_signed[7:0];


	delay_line #( .N(3), .DELAY(8) ) 
	delay_sync (
		.clk(clk),
		.ce(1'b1),
		.rst(1'b0),
		.idata({de_in, hsync_in, vsync_in}),				//[N-1:0]
		.odata({de_out, hsync_out, vsync_out})				//[N-1:0]
	);


endmodule


